package com.weather.myweatherapp.constants;

public class Constants {

    private static final String API_KEY = "8009edea4c2713b096c4186b27269dda";

    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String END_POINT_CURRENT = "weather?appid=" + API_KEY + "&units=metric";
    public static final String END_POINT_FORECAST = "forecast?appid=" + API_KEY + "&units=metric";

    public static final String DATE_FORAMT = "yyyy-MM-dd HH:mm:ss";
}

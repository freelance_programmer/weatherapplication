package com.weather.myweatherapp.viewmodels;

import android.app.Application;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.weather.myweatherapp.repositroy.LocationRepository;

public class LocationViewModel extends AndroidViewModel {

    private LocationRepository mLocationRepository;
    private LiveData<Location> locationLiveData = new MutableLiveData<>();

    public LocationViewModel(@NonNull Application application) {
        super(application);
    }

    public void requestLocationUpdate() {
        if (mLocationRepository == null) {
            mLocationRepository = new LocationRepository(getApplication());
        }
        locationLiveData = mLocationRepository.getLocationChanges();
    }

    public LiveData<Location> getLocationUpdates() {
        return locationLiveData;
    }
}

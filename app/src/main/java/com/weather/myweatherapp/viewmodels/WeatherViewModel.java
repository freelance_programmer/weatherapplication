package com.weather.myweatherapp.viewmodels;

import android.location.Location;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.weather.myweatherapp.models.current.WeatherModel;
import com.weather.myweatherapp.models.forecast.ForecastWeatherModel;
import com.weather.myweatherapp.repositroy.WeatherRepository;

public class WeatherViewModel extends ViewModel {

    private WeatherRepository mWeatherRepository;

    private LiveData<WeatherModel> mCurrentWeatherByLocationLiveData = new MutableLiveData<>();
    private LiveData<WeatherModel> mCurrentWeatherByCityNameLiveData = new MutableLiveData<>();
    private LiveData<ForecastWeatherModel> mForecastWeatherLiveData = new MutableLiveData<>();

    public void requestCurrentWeather(Location location) {
        if(mWeatherRepository == null) {
            mWeatherRepository = new WeatherRepository();
        }
        mCurrentWeatherByLocationLiveData = mWeatherRepository.requestCurrentWeather(location);
    }

    public LiveData<WeatherModel> getCurrentWeather() {
        return mCurrentWeatherByLocationLiveData;
    }

    public void requestCurrentWeatherByCityName(String cityName) {
        if(mWeatherRepository == null) {
            mWeatherRepository = new WeatherRepository();
        }
        mCurrentWeatherByCityNameLiveData = mWeatherRepository.requestCurrentWeatherByCityName(cityName);
    }

    public LiveData<WeatherModel> getCurrentWeatherByCityName() {
        return mCurrentWeatherByCityNameLiveData;
    }

    public void requestForecastWeather(String cityName) {
        if(mWeatherRepository == null) {
            mWeatherRepository = new WeatherRepository();
        }
        mForecastWeatherLiveData = mWeatherRepository.requestForecastWeather(cityName);
    }

    public LiveData<ForecastWeatherModel> getForecastWeather() {
        return mForecastWeatherLiveData;
    }
}

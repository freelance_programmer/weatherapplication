package com.weather.myweatherapp.repositroy;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.weather.myweatherapp.models.forecast.ForecastWeatherModel;
import com.weather.myweatherapp.remote.ApiClient;
import com.weather.myweatherapp.remote.WeatherApiService;
import com.weather.myweatherapp.models.current.WeatherModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherRepository {

    private MutableLiveData<WeatherModel> mutableLiveData = new MutableLiveData<>();
    private MutableLiveData<WeatherModel> mutableLiveDataByCityName = new MutableLiveData<>();
    private MutableLiveData<ForecastWeatherModel> forecastMutableLiveData = new MutableLiveData<>();

    public LiveData<WeatherModel> requestCurrentWeather(Location location) {
        WeatherApiService weatherApiService = ApiClient.getClient().create(WeatherApiService.class);
        weatherApiService.getForecastWeatherByLocation(location.getLatitude(), location.getLongitude())
                .enqueue(new Callback<WeatherModel>() {
                    @Override
                    public void onResponse(@NonNull Call<WeatherModel> call, @NonNull Response<WeatherModel> response) {
                        mutableLiveData.postValue(response.body());
                    }

                    @Override
                    public void onFailure(@NonNull Call<WeatherModel> call, Throwable t) {

                    }
                });

        return mutableLiveData;
    }

    public LiveData<ForecastWeatherModel> requestForecastWeather(String cityName) {
        WeatherApiService weatherApiService = ApiClient.getClient().create(WeatherApiService.class);
        weatherApiService.getForecastData(cityName).enqueue(new Callback<ForecastWeatherModel>() {
            @Override
            public void onResponse(@NonNull Call<ForecastWeatherModel> call, @NonNull Response<ForecastWeatherModel> response) {
                forecastMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<ForecastWeatherModel> call, @NonNull Throwable t) {

            }
        });
        return forecastMutableLiveData;
    }

    public LiveData<WeatherModel> requestCurrentWeatherByCityName(String cityName) {
        WeatherApiService weatherApiService = ApiClient.getClient().create(WeatherApiService.class);
        weatherApiService.getForecastWeatherByCityName(cityName).enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(@NonNull Call<WeatherModel> call, @NonNull Response<WeatherModel> response) {
                mutableLiveDataByCityName.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<WeatherModel> call, @NonNull Throwable t) {

            }
        });
        return mutableLiveDataByCityName;
    }
}

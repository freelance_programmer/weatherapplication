package com.weather.myweatherapp.repositroy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Looper;

import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class LocationRepository implements LocationListener {

    private static final long INTERVAL = 60 * 1000;

    private MutableLiveData<Location> mLocationMutableLiveData = new MutableLiveData<>();

    public LocationRepository(Context context) {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(INTERVAL);
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        mLocationMutableLiveData.setValue(location);
                    }
                }
            }
        };

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.getMainLooper());

        mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if (location != null) {
                mLocationMutableLiveData.setValue(location);
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLocationMutableLiveData == null) {
            mLocationMutableLiveData = new MutableLiveData<>();
        }
        mLocationMutableLiveData.setValue(location);
    }

    public MutableLiveData<Location> getLocationChanges() {
        return mLocationMutableLiveData;
    }
}

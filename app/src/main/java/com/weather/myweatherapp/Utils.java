package com.weather.myweatherapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import permissions.dispatcher.PermissionRequest;

public class Utils {
    public static void showPermissionDialog(Context context, String message, PermissionRequest request) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.allow_text), (dialog, button) -> request.proceed())
                .setNegativeButton(context.getString(R.string.deny_text), (dialog, button) -> request.cancel())
                .show();
    }

    public static void showPermissionDialog(Context context, String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.deny_text), (dialog, button) -> {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", "com.weather.myweatherapp", null);
                    intent.setData(uri);
                    context.startActivity(intent);
                })
                .show();
    }

    private static void showAlertMessageNoGps(Context context) {
        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.deny_text))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.yes_text), (dialog, id) -> context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton(context.getString(R.string.no_text), (dialog, id) -> dialog.cancel()).create().show();
    }

    public static void checkLocationStatus(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showAlertMessageNoGps(context);
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}

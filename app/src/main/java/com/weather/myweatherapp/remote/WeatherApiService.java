package com.weather.myweatherapp.remote;

import com.weather.myweatherapp.constants.Constants;
import com.weather.myweatherapp.models.current.WeatherModel;
import com.weather.myweatherapp.models.forecast.ForecastWeatherModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApiService {

    @GET(Constants.END_POINT_CURRENT)
    Call<WeatherModel> getForecastWeatherByLocation(
            @Query("lat") Double lat,
            @Query("lon") Double lon);

    @GET(Constants.END_POINT_CURRENT)
    Call<WeatherModel> getForecastWeatherByCityName(@Query("q") String cityName);

    @GET(Constants.END_POINT_FORECAST)
    Call<ForecastWeatherModel> getForecastData(@Query("q") String cityName);
}

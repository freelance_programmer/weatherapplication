package com.weather.myweatherapp.view.activity;

import android.Manifest;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.weather.myweatherapp.Converter;
import com.weather.myweatherapp.R;
import com.weather.myweatherapp.Utils;
import com.weather.myweatherapp.adapters.CurrentWeatherAdapter;
import com.weather.myweatherapp.adapters.ForecastWeatherAdapter;
import com.weather.myweatherapp.adapters.base.RecyclerViewBaseAdapter;
import com.weather.myweatherapp.constants.Constants;
import com.weather.myweatherapp.databinding.ActivityWeatherBinding;
import com.weather.myweatherapp.interfaces.OnRecyclerObjectClickListener;
import com.weather.myweatherapp.models.forecast.ForecastWeatherModel;
import com.weather.myweatherapp.viewmodels.LocationViewModel;
import com.weather.myweatherapp.viewmodels.WeatherViewModel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.RuntimePermissions;
import permissions.dispatcher.PermissionRequest;

@RuntimePermissions
@RequiresApi(api = Build.VERSION_CODES.O)
public class WeatherActivity extends AppCompatActivity implements OnRecyclerObjectClickListener<ForecastWeatherModel.WeatherList> {

    private WeatherViewModel weatherViewModel;
    private LocationViewModel locationViewModel;
    private CurrentWeatherAdapter currentWeatherAdapter;
    private ForecastWeatherAdapter forecastWeatherAdapter;

    private ActivityWeatherBinding weatherBinding;
    private SearchView.SearchAutoComplete searchAutoComplete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        weatherBinding = DataBindingUtil.setContentView(this, R.layout.activity_weather);
        weatherBinding.setConverter(new Converter());

        initViewModels();
        WeatherActivityPermissionsDispatcher.requestLocationUpdateWithPermissionCheck(this);

        configureSearchView();

        RecyclerView todayRecyclerView = findViewById(R.id.recycler_view_today);
        currentWeatherAdapter = new CurrentWeatherAdapter(this);
        currentWeatherAdapter.setListener(this);
        configureRecyclerViews(currentWeatherAdapter, todayRecyclerView);

        RecyclerView forecastRecyclerView = findViewById(R.id.recycler_view_five_days);
        forecastWeatherAdapter = new ForecastWeatherAdapter(this);
        forecastWeatherAdapter.setListener(this);
        configureRecyclerViews(forecastWeatherAdapter, forecastRecyclerView);
    }

    private void configureSearchView() {
        SearchView searchView = findViewById(R.id.search_view);
        searchAutoComplete = searchView.findViewById(R.id.search_src_text);
        searchAutoComplete.setBackgroundColor(Color.TRANSPARENT);
        searchAutoComplete.setTextColor(Color.WHITE);
        searchAutoComplete.setDropDownBackgroundResource(android.R.color.white);
        ArrayAdapter<String> newsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.cities));
        searchAutoComplete.setAdapter(newsAdapter);

        searchAutoComplete.setOnItemClickListener((adapterView, view, itemIndex, id) -> {
            String cityName = (String) adapterView.getItemAtPosition(itemIndex);
            searchAutoComplete.setText(cityName);
            requestCurrentWeatherByCityName(cityName);
        });
    }

    private void configureRecyclerViews(RecyclerViewBaseAdapter adapter, RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    private void initViewModels() {
        weatherViewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel.class);
    }

    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    public void requestLocationUpdate() {
        Utils.checkLocationStatus(this);
        locationViewModel.requestLocationUpdate();
        locationViewModel.getLocationUpdates().observe(this, location -> {
            if (searchAutoComplete.getText().toString().equals("")) {
                requestCurrentWeather(location);
            }
        });
    }

    private void requestCurrentWeather(Location location) {
        weatherViewModel.requestCurrentWeather(location);
        weatherViewModel.getCurrentWeather().observe(this, weatherModel -> {
            weatherBinding.setWeatherModel(weatherModel);
            weatherBinding.setWeather(weatherModel.getWeather().get(0));
            requestForecastWeather(weatherModel.getName());
        });
    }

    private void requestForecastWeather(String cityName) {
        weatherViewModel.requestForecastWeather(cityName);
        weatherViewModel.getForecastWeather().observe(this, forecastWeatherModel -> {
            if (forecastWeatherModel != null) {

                ArrayList<ForecastWeatherModel.WeatherList> currentDayList = new ArrayList<>();
                ArrayList<ForecastWeatherModel.WeatherList> forecastWeatherList = new ArrayList<>();

                for (ForecastWeatherModel.WeatherList weather : forecastWeatherModel.getList()) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_FORAMT);
                    String formatDateTime = LocalDateTime.now().format(formatter);
                    if (LocalDateTime.parse(weather.getDtTxt(), formatter).toString().substring(0, formatDateTime.length() - 9).equals(formatDateTime.substring(0, formatDateTime.length() - 9))) {
                        weather.setToday(true);
                        currentDayList.add(weather);
                    } else {
                        forecastWeatherList.add(weather);
                    }
                }
                currentWeatherAdapter.setItems(currentDayList);
                forecastWeatherAdapter.setItems(forecastWeatherList);
            }
        });
    }

    private void requestCurrentWeatherByCityName(String cityName) {
        Utils.hideSoftKeyboard(this);
        weatherViewModel.requestCurrentWeatherByCityName(cityName);
        weatherViewModel.getCurrentWeatherByCityName().observe(this, weatherModel -> weatherBinding.setWeatherModel(weatherModel));
        requestForecastWeather(cityName);
    }

    @OnShowRationale({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showRationaleForLocation(PermissionRequest request) {
        Utils.showPermissionDialog(this, getResources().getString(R.string.allow_location_permission_text), request);
    }

    @OnPermissionDenied({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showPermissionDeniedForLocation() {
        finish();
    }

    @OnNeverAskAgain({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showNeverAskForLocation() {
        Utils.showPermissionDialog(this, getResources().getString(R.string.need_location_permission_text));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        WeatherActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public void onItemClicked(ForecastWeatherModel.WeatherList item) {

    }
}

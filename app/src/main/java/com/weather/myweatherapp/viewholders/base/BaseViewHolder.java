package com.weather.myweatherapp.viewholders.base;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.weather.myweatherapp.interfaces.BaseRecyclerListener;

public abstract class BaseViewHolder<ItemType, L extends BaseRecyclerListener> extends RecyclerView.ViewHolder {

    public BaseViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
    }

    public abstract void onBind(ItemType item, @Nullable L listener, Context context);
}
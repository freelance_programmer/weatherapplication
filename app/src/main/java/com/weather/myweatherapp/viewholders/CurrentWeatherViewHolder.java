package com.weather.myweatherapp.viewholders;

import android.content.Context;

import androidx.annotation.Nullable;

import com.weather.myweatherapp.Converter;
import com.weather.myweatherapp.databinding.RecyclerViewItemBinding;
import com.weather.myweatherapp.interfaces.OnRecyclerObjectClickListener;
import com.weather.myweatherapp.models.forecast.ForecastWeatherModel;
import com.weather.myweatherapp.viewholders.base.BaseViewHolder;

public class CurrentWeatherViewHolder extends BaseViewHolder<ForecastWeatherModel.WeatherList, OnRecyclerObjectClickListener<ForecastWeatherModel.WeatherList>> {

    private RecyclerViewItemBinding binding;

    public CurrentWeatherViewHolder(RecyclerViewItemBinding binding) {
        super(binding);
        this.binding = binding;
    }

    @Override
    public void onBind(final ForecastWeatherModel.WeatherList item, @Nullable final OnRecyclerObjectClickListener<ForecastWeatherModel.WeatherList> listener, Context context) {
        binding.setWeatherforecast(item);
        binding.setWeather(item.getWeather().get(0));
        binding.setConverter(new Converter());
    }
}

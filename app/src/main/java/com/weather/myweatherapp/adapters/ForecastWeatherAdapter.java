package com.weather.myweatherapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.weather.myweatherapp.R;
import com.weather.myweatherapp.adapters.base.RecyclerViewBaseAdapter;
import com.weather.myweatherapp.databinding.RecyclerViewItemBinding;
import com.weather.myweatherapp.interfaces.OnRecyclerObjectClickListener;
import com.weather.myweatherapp.models.forecast.ForecastWeatherModel;
import com.weather.myweatherapp.viewholders.ForecastWeatherViewHolder;

public class ForecastWeatherAdapter extends RecyclerViewBaseAdapter<ForecastWeatherModel.WeatherList, OnRecyclerObjectClickListener<ForecastWeatherModel.WeatherList>, ForecastWeatherViewHolder> {

    public ForecastWeatherAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    public ForecastWeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerViewItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_item, parent, false);
        return new ForecastWeatherViewHolder(binding);
    }
}

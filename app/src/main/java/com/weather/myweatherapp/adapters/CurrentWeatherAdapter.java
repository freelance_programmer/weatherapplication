package com.weather.myweatherapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.weather.myweatherapp.R;
import com.weather.myweatherapp.adapters.base.RecyclerViewBaseAdapter;
import com.weather.myweatherapp.databinding.RecyclerViewItemBinding;
import com.weather.myweatherapp.interfaces.OnRecyclerObjectClickListener;
import com.weather.myweatherapp.models.forecast.ForecastWeatherModel;
import com.weather.myweatherapp.viewholders.CurrentWeatherViewHolder;

public class CurrentWeatherAdapter extends RecyclerViewBaseAdapter<ForecastWeatherModel.WeatherList, OnRecyclerObjectClickListener<ForecastWeatherModel.WeatherList>, CurrentWeatherViewHolder> {

    public CurrentWeatherAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    public CurrentWeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerViewItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_item, parent, false);
        return new CurrentWeatherViewHolder(binding);
    }
}
